const axios = require('axios');
const { Client, logger } = require('camunda-external-task-client-js');
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');
const { Variables } = require("camunda-external-task-client-js");


// ################ Keycloak ##########################
let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}
// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 };

// create a Client instance with custom configuration
const client = new Client(config);

const host = 'http://localhost';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5hbHl0aWNzX3VzZXIifQ.Q8H_V6qh9w-mxHUY4_2IqmWNk_UdkbtacaOAsbP8peE'

client.subscribe('bp-team-get-employee-data', async function({ task, taskService }) {
  axios({
    method: 'get',
    url: host+':3000/bp_employee',
    auth: {
      'bearer': token
    }
  })
  .then(function(response){
    if(response.data){
      let employees = response.data.map(user => ({
        id: user.id,
        f_name: user.f_name,
        l_name: user.l_name,
        m_name: user.m_name,
        birthDate: user.birth_date,
        iin: user.iin,
        idCard: user.id_card,
        position: user.position,
        enterDate: user.enter_date,
        docs: user.docs,
      }))
      return Promise.resolve(employees)
    }
  })
  .catch(function(error){
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
  })
  .then(function(employees){
    const variables = new Variables().setAllTyped({
      employees: {
        value: employees,
        type: 'Json',
      },
    }) 
    taskService.complete(task, variables).then((result) => {
      console.log(`Result: ${result}`)
    })
  })
});

client.subscribe('bp-team-get-team-data', async function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  let initiator = typedValues.initiator.value;
  axios({
    method: 'get',
    url: host+':3000/bp_team?manager_id=eq.'+initiator,
    auth: {
      'bearer': token
    }
  })
  .then(function(response){
    if(response.data){
      let teams = response.data.map(team => ({
        id: team.id,
        name: team.name,
        manager_id: team.managerId,
        docs: team.docs,
        employees: [],
      }))
      return Promise.resolve(teams)
    }
    //return Promise.resolve(employees=[])  
  })
  .catch(function(error){
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
  })
  .then(function(teams){
    const variables = new Variables().setAllTyped({
      teams: {
        value: teams,
        type: 'Json',
      },
    }) 
    taskService.complete(task, variables).then((result) => {
      console.log(`Result: ${result}`)
    })
  })
});

client.subscribe('bp-team-get-membership-data', async function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  let teams = typedValues.teams.value;
  searchTeamsId = '('
  teams.forEach((element,index) => {
    if (index != teams.lenght-1) {
      searchTeamsId += element.id + ','
    } else {
      searchTeamsId += element.id
    } 
  })
  searchTeamsId +=')'
  console.log(searchTeamsId)
  let employees = typedValues.employees.value;
  axios({
    method: 'get',
    url: host+':3000/bp_membership?team_id=in.'+searchTeamsId,
    auth: {
      'bearer': token
    }
  })
  .then(function(response){
    console.log(response)
    if(response.data){
      let memberships = response.data.map(membership => ({
        id: membership.id,
        emp_id: membership.emp_id,
        team_id: membership.team_id,
      }))
      return Promise.resolve(memberships)
    }
    //return Promise.resolve(employees=[])  
  })
  .catch(function(error){
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
  })
  .then(function(memberships){
    employees.forEach(employee => {
      memberships.forEach(membership => {
        if (employee.id == membership.emp_id) {
          teams.forEach(team => {
            if (team.id == membership.team_id) {
              team.employees.push(employee)
            }
          })
        }
      })
    })
    const variables = new Variables().setAllTyped({
      teams: {
        value: teams,
        type: 'Json',
      },
    }) 
    taskService.complete(task, variables).then((result) => {
      console.log(`Result: ${result}`)
    })
  })
});
client.subscribe('bp-team-set-data', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  let teams = typedValues.teams.value;
  let initiator = typedValues.initiator.value;
  teams.forEach(team => {
    if (!team.hasOwnProperty('id')){
      axios({
        method: 'post',
        url: host+':3000/bp_team',
        auth: {
          'bearer': token
        },
        data: {
          name: team.name,
          manager_id: initiator,
          docs: team.docs,
        }
      })
      .catch(function(error){
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
      })
      .then(function(team_response){
        str = team_response.headers.location;
        team_id = str.slice(str.search('eq.')+3)

        team.employees.forEach(employee => {
          if (!employee.hasOwnProperty('id')) {
            axios({
              method: 'post',
              url: host+':3000/bp_employee',
              auth: {
                'bearer': token
              },
              data: {
                f_name: employee.f_name,
                l_name: employee.l_name,
                m_name: employee.m_name,
                birth_date: employee.birthDate,
                iin: employee.iin,
                id_card: employee.idCard,
                position: employee.position,
                enter_date: '2017-12-13',
                docs: employee.docs,
              }
            })
            .catch(function(error){
              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
              }
            })
            .then(function(emp_response){
              str = emp_response.headers.location;
              emp_id = str.slice(str.search('eq.')+3)
              axios({
                method: 'post',
                url: host+':3000/bp_membership',
                auth: {
                  'bearer': token
                },
                data: {
                  emp_id: emp_id,
                  team_id: team_id,
                  stard_date: '2018-10-13',
                }
              })
              .catch(function(error){
                if (error.response) {
                  // The request was made and the server responded with a status code
                  // that falls out of the range of 2xx
                  console.log(error.response.data);
                  console.log(error.response.status);
                  console.log(error.response.headers);
                } else if (error.request) {
                  // The request was made but no response was received
                  // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                  // http.ClientRequest in node.js
                  console.log(error.request);
                } else {
                  // Something happened in setting up the request that triggered an Error
                  console.log('Error', error.message);
                }
              })
            })
          } else {
            axios({
              method: 'post',
              url: host+':3000/bp_membership',
              auth: {
                'bearer': token
              },
              data: {
                emp_id: employee.id,
                team_id: team_id,
                stard_date: '2018-10-14',
              }
            })
            .catch(function(error){
              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
              }
            })
          }
        })
      })
    } else {
      team.employees.forEach(employee => {
        if (!employee.hasOwnProperty('id')) {
          axios({
            method: 'post',
            url: host+':3000/bp_employee',
            auth: {
              'bearer': token
            },
            data: {
              f_name: employee.f_name,
              l_name: employee.l_name,
              m_name: employee.m_name,
              birth_date: employee.birthDate,
              iin: employee.iin,
              id_card: employee.idCard,
              position: employee.position,
              enter_date: '2017-12-12',
              docs: employee.docs,
            }
          })
          .catch(function(error){
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
            }
          })
          .then(function(emp_response){
            str = emp_response.headers.location;
            emp_id = str.slice(str.search('eq.')+3)
            axios({
              method: 'post',
              url: host+':3000/bp_membership',
              auth: {
                'bearer': token
              },
              data: {
                emp_id: emp_id,
                team_id: team.id,
                stard_date: '2018-10-16',
              }
            })
            .catch(function(error){
              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
              }
            })
          })
        } else {
          axios({
            method: 'post',
            url: host+':3000/bp_membership',
            auth: {
              'bearer': token
            },
            data: {
              emp_id: employee.id,
              team_id: team.id,
              stard_date: '2018-10-17',
            }
          })
          .catch(function(error){
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
            }
          })
        }
      })
    }
    
  })

  /* let employees = typedValues.newEmployees.value
  employees.forEach(employee => {
    axios({
      method: 'post',
      url: host+':3000/bp_employee',
      auth: {
        'bearer': token
      },
      data: {
        f_name: employee.f_name,
        l_name: employee.l_name,
        m_name: employee.m_name,
        birth_date: employee.birthDate,
        iin: employee.iin,
        id_card: employee.idCard,
        position: employee.position,
        enter_date: employee.enterDate,
        docs: employee.docs,
      }
    })
    .catch(function(error){
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
    })
    .then(function(response){
      console.log(response)
    })
  }) */
 
  await taskService.complete(task);
});
  // Запись в 
/* client.subscribe('set-data-user', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var partner = typedValues.newPerson.value
  var method = typedValues.card.value
  var headId = typedValues.initiator.value
  if (partner){
    if (method == 'change'){
      request({
        method: 'POST',
        uri: host+':3000/persons',
        auth: {
          'bearer': token
        },
        'content-type': 'application/json',
        body: JSON.stringify({
          person_status: partner.status,
          person_fullname: partner.fullName,
          person_birthdate: partner.birthDate,
          person_address: partner.address,
          iin: partner.iin,
          id_card: partner.idCard,
          position: partner.position,
          enter_date: partner.enterDate,
          company: partner.company,
          project: partner.project,
          quality_card: partner.qualityCard,
          insurance: partner.insurance,
          head_id: headId,
        })
      }, function(error, response, body){
        if(error){
          console.log('error:=' + error);
        } else {
          console.log(body)
        }
      })
    } else if (method == 'read') {
      request({
        method: 'PATCH',
        uri: host+':3000/persons?iin=eq.'+partner.iin,
        auth: {
          'bearer': token
        },
        'content-type': 'application/json',
        body: JSON.stringify({
          person_status: 'unchecked',
          insurance: partner.insurance,
          quality_card: partner.qualityCard,
        })
      }, function(error, response, body){
        if(error){
          console.log('error:=' + error);
        } else {
          console.log(body)
        }
      })
    }
  } 
 
  await taskService.complete(task);
});
  // взять новых для БиОТ
client.subscribe('get-unchecked-users', async function({ task, taskService }) {

  axios.get({
    'url': host+':3000/persons?person_status=eq.unchecked',
    'auth': {
      'bearer': token
    }
  }).then(function(response){

    return Promise.resolve(persons)
  }).then(function(persons){

  })
  
  
  , function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(body){
        let persons = JSON.parse(body);
        persons = persons.map(user => ({
          status: user.person_status,
          fullName: user.person_fullname,
          birthDate: user.person_birthdate,
          address: user.person_address,
          iin: user.iin,
          idCard: user.id_card,
          position: user.position,
          enterDate: user.enter_date,
          company: user.company,
          project: user.project,
          qualityCard: user.quality_card,
          insurance: user.insurance,
        }))
        const variables = new Variables().setAllTyped({
          partners: {
            value: persons,
            type: 'Json',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      }
    }
  })
});

client.subscribe('change-status', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var status = typedValues.setStatus.value;
  var partners = typedValues.partners.value;
  var newStatus = '';
  if (partners){
    partners.forEach(partner => 
    {
      newStatus = status === 'processing' ? 'processing' : partner.status;
      request({
        method: 'PATCH',
        uri: host+':3000/persons?iin=eq.'+partner.iin,
        auth: {
          'bearer': token
        },
        'content-type': 'application/json',
        body: JSON.stringify({
          person_status: newStatus,
          comment: partner.comment
        })
      }, function(error, response, body){
        if(error){
          console.log('error:=' + error);
        } else {
          console.log(body)
        }
      })
    })
  } 
  await taskService.complete(task);
});

client.subscribe('get-checked-users', async function({ task, taskService }) {

  const searchStatus = task.variables.getAllTyped().searchStatus.value
  request.get({
    'url': host+':3000/persons?person_status=eq.'+searchStatus,
    'auth': {
      'bearer': token
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(body){
        let persons = JSON.parse(body);
        persons = persons.map(user => ({
          status: user.person_status,
          fullName: user.person_fullname,
          birthDate: user.person_birthdate,
          address: user.person_address,
          iin: user.iin,
          idCard: user.id_card,
          position: user.position,
          enterDate: user.enter_date,
          company: user.company,
          project: user.project,
          qualityCard: user.quality_card,
          insurance: user.insurance,
          id: user.head_id,
        }))
          // Вытащить руководителей
        var headers = []
        persons.forEach(element=>{
            headers.push(element.id)
        })
          // Get unique headers
        var obj = {};
        for (var i = 0; i < headers.length; i++) {
          var str = headers[i];
          obj[str] = true; 
        }
        headers = Object.keys(obj) // array unique HeadId

        var partnersHeaders = [];
        for(var i=0; i < headers.length; i++) {
            var arr = []
            persons.forEach(element => {
                if (headers[i] === element.id) {
                    arr.push(element)
                }
            });
            partnersHeaders.push({headId: headers[i], partners: arr}) // new array partners grouped by
        }
        
        const variables = new Variables().setAllTyped({
          partnersHeaders: {
            value: partnersHeaders,
            type: 'Json',
          },
          checkedPartners: {
            value: persons,
            type: 'Json',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      }
    }
  })
}); */